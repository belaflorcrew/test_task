<?
/* Получаем доступ к сесси и проверяем авторизацию пользователя */
session_start ();
$data = [];
if(isset($_SESSION['login'])){
    $login = $_SESSION['login'];
    $db = simplexml_load_file("libs/db.xml");
    $user;
    foreach ($db->users->user as $all_users) {
        if ($all_users->login  == $login) {
            $user = $all_users;
            break;
        }
    }
    $data['user_name'] = (string)$user->name;
}

header("Content-Type: application/json");
die(json_encode($data))
?>
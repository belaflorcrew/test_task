$(function (){
    
    var $resultsLogin = $("#resultsLogin");  
    var $login_form = $('form[name="login"]');
    var $resultsRegistration = $("#resultsRegistration");
    var $registration_form = $('form[name="registration"]');
    
    $.ajax({
        url: 'check.php',
        type: 'POST',
        success: function(data){
            if (data && data.user_name) {
                $resultsLogin.html('<p class="success">'+'Hello '+data.user_name+'</p>');
            } else {
                $login_form.show();
                $registration_form.show();
            }
        }
    });
    
    $registration_form.on("submit", function(e){
        e.preventDefault();
        $.ajax({
            url: 'registration.php',
            data: $registration_form.serialize(),
            type: 'POST',
            success: function(data){
                var buffer = "";
                if (data.errors) {
                    if(data.errors.wrong_login) {
                        buffer +='<p class="error">'+data.errors.wrong_login+'</p>';
                    }
                    if(data.errors.wrong_email) {
                        buffer +='<p class="error">'+data.errors.wrong_email+'</p>';
                    }
                    if(data.errors.wrong_password) {
                        buffer +='<p class="error">'+data.errors.wrong_password+'</p>';
                    }
                    if(data.errors.login) {
                        buffer +='<p class="error">'+data.errors.login+'</p>';
                    }
                    if(data.errors.password) {
                        buffer +='<p class="error">'+data.errors.password+'</p>';
                    }
                    if(data.errors.confirmpass) {
                        buffer +='<p class="error">'+data.errors.confirmpass+'</p>';
                    }
                    if(data.errors.email) {
                        buffer +='<p class="error">'+data.errors.email+'</p>';
                    }
                    if(data.errors.name) {
                        buffer +='<p class="error">'+data.errors.name+'</p>';
                    }
                }
                if (data.success){
                    buffer +='<p class="success">'+data.success+'</p>';
                }
                $resultsRegistration.html(buffer);
                console.log(data);
            }
        });
    });
    
    
    $login_form.on("submit", function(e){
        e.preventDefault();
        $.ajax({
            url: 'login.php',
            data: $login_form.serialize(),
            type: 'POST',
            success: function(data){
                var buffer = "";
                if (data.errors) {
                    if(data.errors.wrong_login) {
                        buffer +='<p class="error">'+data.errors.wrong_login+'</p>';
                    }
                    if(data.errors.wrong_password) {
                        buffer +='<p class="error">'+data.errors.wrong_password+'</p>';
                    }
                }
                if (data.user_name){
                   buffer +='<p class="success">'+'Hello '+data.user_name+'</p>';
                   $login_form.hide();
                   $registration_form.hide();
                   $resultsRegistration.hide();
                }
                $resultsLogin.html(buffer);
            }
        });
    });
});
                

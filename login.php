<?
session_start ();
/* Защита от инъекций */
$login = htmlspecialchars(strip_tags(trim($_POST['authlogin'])));
$password = htmlspecialchars(strip_tags(trim($_POST['authpass'])));

$data = [];
$errors = [];
/* Подключаемся к БД  */
$db = simplexml_load_file("libs/db.xml");
/* Ищем пользователя по логину */
$user;
foreach ($db->users->user as $all_users) {
    if ($all_users->login  == $login) {
        $user = $all_users;
        break;
    }
}
/* Проверяем ответ и возвращаем ошибку если данные не совпадают */
if(!$user){
   $errors['wrong_login'] = "Пользователя с таким Логином нет в базе"; 
}
if($user->login == $login && $user->password != md5('manao'.$password)){
    $errors['wrong_password'] = "Неверный пароль";
}
/* Если данные совпали, записываем логин в сессию, возвращаем имя пользователя */
if($user->login == $login && $user->password == md5('manao'.$password)){
    $data['user_name'] = (string)$user->name;
    $_SESSION["login"] = $login;
}
if ($errors) {
    $data['errors'] = $errors;
}

header("Content-Type: application/json");
die(json_encode($data))
?>
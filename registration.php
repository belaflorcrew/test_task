<?
/* Защита от инъекций */
$login = htmlspecialchars(strip_tags(trim($_POST['login'])));
$password = htmlspecialchars(strip_tags(trim($_POST['pass'])));
$confirmpass = htmlspecialchars(strip_tags(trim($_POST['confirmpass'])));
$email = htmlspecialchars(strip_tags(trim($_POST['email'])));
$name = htmlspecialchars(strip_tags(trim($_POST['name'])));

$data = [];
$errors = [];
/* Проверяем данные и возвращаем ошибку*/
if(!$login) {
    $errors['login'] = "Введите логин";
}
if(!$password) {
    $errors['password'] = "Введите пароль";
}
if(!$confirmpass) {
    $errors['confirmpass'] = "Повторите пароль";
}
if(!$email) {
    $errors['email'] = "Введите емейл";
}
if(!$name) {
    $errors['name'] = "Напишите свое имя";
}
if($password != $confirmpass) {
    $errors['wrong_password'] = "Пароли не совпадают";
}
/* Подключаемся к БД  */
$db = simplexml_load_file("libs/db.xml");
/* Проверяем на уникальность логин и емейл */
    foreach ($db->users->user as $all_users) {
        if ($all_users->login  == $login) {
            $errors['wrong_login'] = "Логин занят";
        }
        else if ($all_users->email  == $email) {
            $errors['wrong_email'] = "Пользователь с таким емейлом уже есть в базе";
        }
    }
$data['errors'] = $errors;
/* Если в форме нет ошибок, записываем данные в БД */
if (!$errors) {
    $user = $db->users->addChild('user');
    $user->addChild('name', $name);
    $user->addChild('email', $email);
    $user->addChild('login', $login);
    $user->addChild('password', md5('manao'.$password));
    $db->asXml("libs/db.xml");
    $data['success'] = "Регистрация прошла успешно";
}

header("Content-Type: application/json");
die(json_encode($data))
?>